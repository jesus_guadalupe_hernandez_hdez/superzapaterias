﻿using SuperZapatos.Data.Models;
using SuperZapatos.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperZapatos.Services.Procesos
{
    public class UtilStores
    {
        public static List<Stores> getData(List<table_stores> lstArt)
        {
            List<Stores> lstDat = new List<Stores>();
            foreach (table_stores ar in lstArt)
            {
                Stores store = new Stores();
                store.address = ar.address;
                store.id = ar.id;
                store.name = ar.name;
                lstDat.Add(store);
            }
            return lstDat;
        }
    }
}