﻿using SuperZapatos.Data.Models;
using SuperZapatos.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperZapatos.Services.Procesos
{
    public class UtilArticles
    {
        public static List<Articles> getData(List<tablearticles> lstArt) {
            List<Articles> lstDat = new List<Articles>();
            foreach (tablearticles ar in lstArt){
                Articles artc = new Articles();
                artc.description = ar.description;
                artc.id = ar.id;
                artc.name = ar.name;
                artc.price = ar.price;
                artc.store_id = ar.store_id;
                artc.total_in_shelf = ar.total_in_shelf;
                artc.total_in_vault = ar.total_in_vault;
                lstDat.Add(artc);
            }
            return lstDat;
        }
    }
}