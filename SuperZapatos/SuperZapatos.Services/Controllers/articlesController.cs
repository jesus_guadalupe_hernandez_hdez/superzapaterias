﻿using SuperZapatos.Business;
using SuperZapatos.Data.Data;
using SuperZapatos.Data.Models;
using SuperZapatos.Services.Models;
using SuperZapatos.Services.Procesos;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SuperZapatos.Services.Controllers
{
    public class articlesController : ApiController
    {
        // GET api/<controller>
        [SwaggerResponse(HttpStatusCode.OK, type: typeof(ResponseArticles), Description = "Successfull operation")]
        [SwaggerResponse(HttpStatusCode.BadRequest, type: typeof(AppError400), Description = "Bad request")]
        [SwaggerResponse(HttpStatusCode.NotFound, type: typeof(AppError404), Description = "Record not found")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, type: typeof(AppError500), Description = "Server Error")]
        public  async Task<HttpResponseMessage> Get()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new AppError400() { error_msg = "Bad request " });
                }

                List<tablearticles> lstserv = await new TableArticles_Business(new TableArticles_Repository()).getListTableArticles();
                if (lstserv == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, new AppError404());
                }
                ResponseArticles resp = new ResponseArticles();
                resp.articles = UtilArticles.getData(lstserv);
                resp.total_elements = lstserv.Count;
                resp.success = true;
                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex) {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new AppError500() { error_msg = ex.Message });
            }  
        }

        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, type: typeof(ResponseArticles), Description = "Successfull operation")]
        [SwaggerResponse(HttpStatusCode.BadRequest, type: typeof(AppError400), Description = "Bad request")]
        [SwaggerResponse(HttpStatusCode.NotFound, type: typeof(AppError404), Description = "Record not found")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, type: typeof(AppError500), Description = "Server Error")]
        [Route("services/articles/stores")]
        public async Task<HttpResponseMessage> GetDat(  long id) 
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new AppError400() { error_msg  = "Bad request " });
                }

                List<tablearticles> lstserv = await new TableArticles_Business(new TableArticles_Repository()).getListTableArticlesxStore(id);
                if (lstserv == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, new AppError404());
                }
                ResponseArticles resp = new ResponseArticles();
                resp.articles = UtilArticles.getData(lstserv);
                resp.total_elements = lstserv.Count;
                resp.success = true;
                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex) {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new AppError500() { error_msg  = ex.Message});
            }
        }

        // GET api/<controller>/5
        [SwaggerResponse(HttpStatusCode.OK, type: typeof(tablearticles), Description = "Successfull operation")]
        [SwaggerResponse(HttpStatusCode.BadRequest, type: typeof(AppError400), Description = "Bad request")]
        [SwaggerResponse(HttpStatusCode.NotFound, type: typeof(AppError404), Description = "Record not found")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, type: typeof(AppError500), Description = "Server Error")]
        public async Task<HttpResponseMessage> Get(long id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new AppError400() { error_msg = "Bad request " });
                }
                tablearticles art = new tablearticles();
                art.id = id;
                tablearticles serv = await new TableArticles_Business(new TableArticles_Repository()).getTableArticles(art);
                if (serv == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new AppError404());
                }
                return Request.CreateResponse(HttpStatusCode.OK, serv);
            }
            catch (Exception ex) {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new AppError500() { error_msg = ex.Message });
            }
            
        }

        // POST api/<controller>
        [SwaggerResponse(HttpStatusCode.OK, type: typeof(string), Description = "Successfull operation")]
        [SwaggerResponse(HttpStatusCode.BadRequest, type: typeof(AppError400), Description = "Bad request")]
        [SwaggerResponse(HttpStatusCode.NotFound, type: typeof(AppError404), Description = "Record not found")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, type: typeof(AppError500), Description = "Server Error")]
        public async Task<HttpResponseMessage> Post(tablearticles arti)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new AppError400() { error_msg = "Bad request " });
                }

                var tem = await new TableArticles_Business(new TableArticles_Repository()).saveTableArticles(arti);
                
                if (tem == 1)
                    return Request.CreateResponse(HttpStatusCode.OK, "SUCESS");
                else
                    return Request.CreateResponse(HttpStatusCode.OK, "Error");
            }
            catch (Exception ex) {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new AppError500() { error_msg = ex.Message });
            }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}