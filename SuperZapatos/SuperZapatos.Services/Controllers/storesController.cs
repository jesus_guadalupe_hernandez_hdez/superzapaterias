﻿using SuperZapatos.Business;
using SuperZapatos.Data.Data;
using SuperZapatos.Data.Models;
using SuperZapatos.Services.Models;
using SuperZapatos.Services.Procesos;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SuperZapatos.Services.Controllers
{
    public class storesController : ApiController
    {
        [SwaggerResponse(HttpStatusCode.OK, type :typeof(ResponseStores), Description = "Successfull operation")]
        [SwaggerResponse(HttpStatusCode.BadRequest,type: typeof(AppError400), Description = "Bad request")]
        [SwaggerResponse(HttpStatusCode.NotFound, type: typeof(AppError404), Description = "Record not found")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, type: typeof(AppError500), Description = "Server Error")]
        public async Task<HttpResponseMessage> Get()
        {
            try {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new AppError400() { error_msg = "Bad request " });
                }
                List<table_stores> lstStore = await new Table_Stores_Business(new Table_Stores_Repository()).getListTableStore();
                if (lstStore == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, new AppError404());
                }
                ResponseStores resp = new ResponseStores();
                resp.stores = UtilStores.getData(lstStore);
                resp.total_elements = lstStore.Count;
                resp.success = true;
                return Request.CreateResponse(HttpStatusCode.OK, resp);
            }
            catch (Exception ex) {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new AppError500() { error_msg = ex.Message });
            }
            
        }

        // GET api/<controller>/5
        [SwaggerResponse(HttpStatusCode.OK, type: typeof(table_stores), Description = "Successfull operation")]
        [SwaggerResponse(HttpStatusCode.BadRequest, type: typeof(AppError400), Description = "Bad request")]
        [SwaggerResponse(HttpStatusCode.NotFound, type: typeof(AppError404), Description = "Record not found")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, type: typeof(AppError500), Description = "Server Error")]
        public async Task<HttpResponseMessage> Get(int id)
        {
            try {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new AppError400() { error_msg = "Bad request " });
                }
                table_stores store = new table_stores();
                store.id = id;
                table_stores storee = await new Table_Stores_Business(new Table_Stores_Repository()).getTableStores(store);
                if (storee == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, new AppError404());
                }
                return Request.CreateResponse(HttpStatusCode.OK, storee);
            }
            catch (Exception ex) {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new AppError500() { error_msg = ex.Message });
            }
            
        }

        // POST api/<controller>
        [SwaggerResponse(HttpStatusCode.OK, type: typeof(string), Description = "Successfull operation")]
        [SwaggerResponse(HttpStatusCode.BadRequest, type: typeof(AppError400), Description = "Bad request")]
        [SwaggerResponse(HttpStatusCode.NotFound, type: typeof(AppError404), Description = "Record not found")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, type: typeof(AppError500), Description = "Server Error")]
        public async Task<HttpResponseMessage> Post(table_stores value)
        {
            try {
                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new AppError400() { error_msg = "Bad request " });
                }
                var tem = await new Table_Stores_Business(new Table_Stores_Repository()).saveTableStores(value);
                if (tem == 1)
                    return Request.CreateResponse(HttpStatusCode.OK, "SUCESS");
                else
                    return Request.CreateResponse(HttpStatusCode.OK, "Error");
            }
            catch (Exception ex) {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new AppError500() { error_msg = ex.Message });
            }
            
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}