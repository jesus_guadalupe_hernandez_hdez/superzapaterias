﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperZapatos.Services.Models
{
    public class AppError400
    {
        public bool success { get { return false; }  } 
        public string error_code { get { return "400"; } }
        public string error_msg { get; set; }
    }
}