﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperZapatos.Services.Models
{
    public class ResponseArticles
    {
        public List<Articles> articles;
        public bool success { get; set; }
        public int total_elements { get; set; }
    }
}