﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperZapatos.Services.Models
{
    public class ResponseStores
    {
        public List<Stores> stores;
        public bool success { get; set; }
        public int total_elements;
    }
}