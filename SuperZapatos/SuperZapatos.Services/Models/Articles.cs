﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperZapatos.Services.Models
{
    public class Articles
    {
        public long id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public decimal price { get; set; }
        public decimal? total_in_shelf { get; set; }
        public decimal? total_in_vault { get; set; }
        public long store_id { get; set; }
    }
}