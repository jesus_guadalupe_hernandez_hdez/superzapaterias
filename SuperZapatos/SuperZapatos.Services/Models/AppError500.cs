﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperZapatos.Services.Models
{
    public class AppError500
    {
        public bool success { get { return false; } }
        public string error_code { get { return "500"; } }
        public string error_msg { get; set; }
    }
}