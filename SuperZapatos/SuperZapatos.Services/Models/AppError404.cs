﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperZapatos.Services.Models
{
    public class AppError404
    {
        public bool success { get { return false; } }
        public string error_code { get { return "404"; } }
        public string error_msg { get { return "Record not found"; } }
    }
}