﻿using SuperZapatos.Data.Models;
using SuperZapatos.Repository.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Business
{
    public class Table_Stores_Business
    {
        ITable_Stores_Repository<table_stores> rep;
        public Table_Stores_Business(ITable_Stores_Repository<table_stores> _rep)
        {
            rep = _rep;
        }

        public async Task<table_stores> getTableStores(table_stores store)
        {
            return await rep.getTableStores(store);
        }

        public async Task<int> saveTableStores(table_stores store)
        {
            return await rep.saveTableStores(store);
        }

        public async Task<int> updateTableStores(table_stores store)
        {
            return await rep.updateTableStores(store);
        }

        public async Task<int> deleteTableStores(table_stores store)
        {
            return await rep.deleteTableStores(store);
        }

        public async Task<List<table_stores>> getListTableStore() {
            return await rep.getListTableStore();
        }
    }
}
