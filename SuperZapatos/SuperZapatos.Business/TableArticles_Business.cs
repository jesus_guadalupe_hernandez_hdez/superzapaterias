﻿using SuperZapatos.Data.Models;
using SuperZapatos.Repository.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Business
{
    public class TableArticles_Business
    {
        ITableArticles_Repository<tablearticles> rep;

        public TableArticles_Business(ITableArticles_Repository<tablearticles> _rep)
        {
            rep = _rep;
        }

        public async Task<tablearticles> getTableArticles(tablearticles store)
        {
            return await rep.getTableArticles(store);
        }

        public async Task<int> saveTableArticles(tablearticles store)
        {
            return await rep.saveTableArticles(store);
        }

        public async Task<int> updateTableArticles(tablearticles store)
        {
            return await rep.updateTableArticles(store);
        }

        public async Task<int> deleteTableArticles(tablearticles store)
        {
            return await deleteTableArticles(store);
        }

        public async Task<List<tablearticles>> getListTableArticles()
        {
            return await rep.getListTableArticles();
        }

        public async Task<List<tablearticles>> getListTableArticlesxStore(long idStore)
        {
            return await rep.getListTableArticlesxStore(idStore);
        }

        
    }
}