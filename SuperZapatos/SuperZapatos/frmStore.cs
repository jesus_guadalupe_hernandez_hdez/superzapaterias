﻿using Newtonsoft.Json;
using SuperZapatos.Business;
using SuperZapatos.Services.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SuperZapatos
{
    public partial class frmStore : Form
    {
        private static ResponseStores resp;
        public frmStore()
        {
            InitializeComponent();
            fillData();
        }
        private void fillData()
        {
            resp = StoreBusiness.getResponseStore();
            fillGrid(resp.stores);
        }

        private void fillGrid(List<Stores> lstDat)
        {
            dataGridStore.DataSource = null;
            dataGridStore.Rows.Clear();
            dataGridStore.DataSource = lstDat;
            dataGridStore.Columns[0].Width = 100;
            dataGridStore.Columns[1].Width = 400;
            dataGridStore.Columns[2].Width = 700;
            dataGridStore.Columns[0].HeaderText = "ID";
            dataGridStore.Columns[1].HeaderText = "Tienda";
            dataGridStore.Columns[2].HeaderText = "Dirección";
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            if (resp.total_elements >= 1)
            {
                var list_join = (List<Stores>)(from a in resp.stores
                                               where a.address.Contains(@txtBuscar.Text) ||
                                               a.name.Contains(@txtBuscar.Text)
                                               select a).ToList();

                fillGrid(list_join);
            }
            if (txtBuscar.Text.Trim().Length == 0)
            {
                fillGrid(resp.stores);
            }
        }
    }
}
