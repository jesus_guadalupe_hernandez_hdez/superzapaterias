﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SuperZapatos
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void storeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmStore frmSto = new frmStore();
            frmSto.MdiParent = this;
            frmSto.Show();
        }

        private void articleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmArticles frmAr = new frmArticles();
            frmAr.MdiParent = this;
            frmAr.Show();
        }
    }
}
