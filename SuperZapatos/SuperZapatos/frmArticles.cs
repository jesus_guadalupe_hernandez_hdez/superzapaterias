﻿using Newtonsoft.Json;
using SuperZapatos.Business;
using SuperZapatos.Services.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SuperZapatos
{
    public partial class frmArticles : Form
    {
        private static ResponseArticles resp;

        public frmArticles()
        {
            InitializeComponent();
            fillCombo();
            fillData();
            
        }

        private void fillCombo() {
            ResponseStores _resp = StoreBusiness.getResponseStore();
            Stores stor = new Stores();
            stor.id = 0;
            stor.name = "Todos";
            _resp.stores.Add(stor);

            this.comboBox1.DataSource = _resp.stores;
            this.comboBox1.DisplayMember = "name";
            this.comboBox1.ValueMember = "id";

            comboBox1.Text= "Todos";
        }

        private void fillData() {
            resp = ArticleBusiness.getResponseArticle();
            fillGrid(resp.articles);
        }

        private void fillGrid(List<Articles> lstDat ) {
            dataGridStore.DataSource = null;
            dataGridStore.Rows.Clear();
            dataGridStore.DataSource = lstDat;
            dataGridStore.Columns[0].Width = 80;
            dataGridStore.Columns[1].Width = 250;
            dataGridStore.Columns[2].Width = 400;
            dataGridStore.Columns[3].Width = 120;
            dataGridStore.Columns[4].Width = 120;
            dataGridStore.Columns[5].Width = 120;

            dataGridStore.Columns[0].HeaderText = "ID";
            dataGridStore.Columns[1].HeaderText = "Nombre";
            dataGridStore.Columns[2].HeaderText = "Descripción";
            dataGridStore.Columns[3].HeaderText = "Precio";
            dataGridStore.Columns[4].HeaderText = "Total en Estante";
            dataGridStore.Columns[5].HeaderText = "Total en Vault";
            dataGridStore.Columns[6].Visible = false;
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            if (resp!= null) {
                if (resp.total_elements >= 1) {
                    var list_join = (List<Articles>)(from a in resp.articles
                                                     where a.name.Contains(@txtBuscar.Text) ||
                                                     a.description.Contains(@txtBuscar.Text)
                                                     select a).ToList();

                    fillGrid(list_join);
                }
                if (txtBuscar.Text.Trim().Length == 0) {
                    fillGrid(resp.articles);
                }
            }
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            Stores tem = (Stores) comboBox1.SelectedItem;

            if (tem.id != 0 && !comboBox1.Text.Equals("Todos")) {
                resp = ArticleBusiness.getResponseArticlexStore(tem.id);
                if (resp != null && resp.total_elements > 0)
                    fillGrid(resp.articles);
                else
                {
                    dataGridStore.DataSource = null;
                    dataGridStore.Rows.Clear();
                }
            }
            else if (tem.id ==0) {
                fillData();
            }
        }
    }
}
