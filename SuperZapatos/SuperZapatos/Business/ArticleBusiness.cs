﻿using Newtonsoft.Json;
using SuperZapatos.Services.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Business
{
    public class ArticleBusiness
    {
        public static ResponseArticles getResponseArticle()
        {
            try
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage response = client.GetAsync("http://localhost/services/articles").Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string rt = response.Content.ReadAsStringAsync().Result;
                    ResponseArticles resp = JsonConvert.DeserializeObject<ResponseArticles>(rt);
                    return resp;
                }
                else if (response.StatusCode == HttpStatusCode.NotFound && response.StatusCode == HttpStatusCode.Forbidden && response.StatusCode == HttpStatusCode.BadRequest && response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static ResponseArticles getResponseArticlexStore(long id)
        {
            try
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage response = client.GetAsync("http://localhost/services/articles/stores?id=" + id).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string rt = response.Content.ReadAsStringAsync().Result;
                    ResponseArticles resp = JsonConvert.DeserializeObject<ResponseArticles>(rt);
                    return resp;
                }
                else if (response.StatusCode == HttpStatusCode.NotFound && response.StatusCode == HttpStatusCode.Forbidden && response.StatusCode == HttpStatusCode.BadRequest && response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
