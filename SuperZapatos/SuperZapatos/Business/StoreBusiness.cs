﻿using Newtonsoft.Json;
using SuperZapatos.Services.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Business
{
    public class StoreBusiness
    {
        public static ResponseStores getResponseStore() {
            try {
                HttpClient client = new HttpClient();
                HttpResponseMessage response = client.GetAsync("http://localhost/services/stores").Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string rt = response.Content.ReadAsStringAsync().Result;
                    ResponseStores resp = JsonConvert.DeserializeObject<ResponseStores>(rt);
                    return resp;
                }
                else if (response.StatusCode == HttpStatusCode.NotFound && response.StatusCode == HttpStatusCode.Forbidden && response.StatusCode == HttpStatusCode.BadRequest && response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex) {
                return null;
            }
            
            /*
            ResponseStores resp;
            var url = $"http://localhost/services/stores";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Accept = "application/json";
            request.Timeout = 3600;
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null)
                            return null;
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();
                            resp = JsonConvert.DeserializeObject<ResponseStores>(responseBody);
                            if (resp.total_elements >= 1)
                            {
                                return resp;
                            }
                        }
                    }
                    return null;
                }
            }
            catch (WebException ex)
            {
                return null;
            }
            */
        }
    }
}
