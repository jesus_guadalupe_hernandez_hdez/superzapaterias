USE [dbZapateria]
GO
/****** Object:  Table [dbo].[table_stores]    Script Date: 31/07/2020 05:13:40 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[table_stores](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [nchar](25) NOT NULL,
	[address] [varchar](250) NOT NULL,
 CONSTRAINT [PK_table_stores] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tablearticles]    Script Date: 31/07/2020 05:13:40 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tablearticles](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [nchar](25) NOT NULL,
	[description] [varchar](200) NOT NULL,
	[price] [decimal](18, 2) NOT NULL,
	[total_in_shelf] [decimal](18, 2) NULL,
	[total_in_vault] [decimal](18, 2) NULL,
	[store_id] [bigint] NOT NULL,
 CONSTRAINT [PK_table articles] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tablearticles]  WITH CHECK ADD  CONSTRAINT [FK_tablearticles_table_stores] FOREIGN KEY([store_id])
REFERENCES [dbo].[table_stores] ([id])
GO
ALTER TABLE [dbo].[tablearticles] CHECK CONSTRAINT [FK_tablearticles_table_stores]
GO
