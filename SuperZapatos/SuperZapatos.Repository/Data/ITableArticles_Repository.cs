﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Repository.Data
{
    public interface ITableArticles_Repository<T>
    {
        Task<T> getTableArticles(T request);
        Task<int> saveTableArticles(T request);
        Task<int> updateTableArticles(T request);
        Task<int> deleteTableArticles(T request);
        Task<List<T>> getListTableArticles();
        Task<List<T>> getListTableArticlesxStore(long idStore);

    }
}
