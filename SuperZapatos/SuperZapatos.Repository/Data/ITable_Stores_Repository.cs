﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Repository.Data
{
    public interface ITable_Stores_Repository<T>
    {
        Task <T> getTableStores(T request);
        Task<int> saveTableStores(T request);
        Task<int> updateTableStores(T request);
        Task<int> deleteTableStores(T request);
        Task<List<T>> getListTableStore();
    }
}
