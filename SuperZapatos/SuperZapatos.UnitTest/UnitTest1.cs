﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SuperZapatos.Business;
using SuperZapatos.Data.Data;
using SuperZapatos.Data.Models;

namespace SuperZapatos.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            getListaStore().Wait();
        }

        [TestMethod]
        public void TestMethod2()
        {
            getListaArticles().Wait();
        }
        
        private static  async Task< List<table_stores>> getListaStore() {
            return await new Table_Stores_Business(new Table_Stores_Repository()).getListTableStore();
            
        }

        private static async Task<List<tablearticles>> getListaArticles()
        {
            return await new  TableArticles_Business(new TableArticles_Repository()).getListTableArticles();
        }
    }
}
