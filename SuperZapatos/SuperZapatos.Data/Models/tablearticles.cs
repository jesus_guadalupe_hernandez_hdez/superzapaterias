namespace SuperZapatos.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tablearticles
    {
        public long id { get; set; }

        [Required]
        [StringLength(25)]
        public string name { get; set; }

        [Required]
        [StringLength(200)]
        public string description { get; set; }

        public decimal price { get; set; }

        public decimal? total_in_shelf { get; set; }

        public decimal? total_in_vault { get; set; }

        public long store_id { get; set; }

      
        public virtual table_stores table_stores { get; set; }
    }
}
