namespace SuperZapatos.Data.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SuperZapatos : DbContext
    {
        public SuperZapatos()
            : base("name=SuperZapatos")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<table_stores> table_stores { get; set; }
        public virtual DbSet<tablearticles> tablearticles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<table_stores>()
                .Property(e => e.name)
                .IsFixedLength();

            modelBuilder.Entity<table_stores>()
                .Property(e => e.address)
                .IsUnicode(false);

            modelBuilder.Entity<table_stores>()
                .HasMany(e => e.tablearticles)
                .WithRequired(e => e.table_stores)
                .HasForeignKey(e => e.store_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tablearticles>()
                .Property(e => e.name)
                .IsFixedLength();

            modelBuilder.Entity<tablearticles>()
                .Property(e => e.description)
                .IsUnicode(false);
        }
    }
}
