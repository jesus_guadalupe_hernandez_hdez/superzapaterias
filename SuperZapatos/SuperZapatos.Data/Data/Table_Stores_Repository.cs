﻿using SuperZapatos.Data.Models;
using SuperZapatos.Repository.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SuperZapatos.Data.Data
{
    public class Table_Stores_Repository : ITable_Stores_Repository<table_stores>
    {
        public async Task<table_stores> getTableStores(table_stores store) {
            using (var db = new SuperZapatos.Data.Models.SuperZapatos()) {
                try
                {
                    var tem = await db.table_stores.Where(x => x.id == store.id).FirstOrDefaultAsync();
                    return tem;
                }
                catch (Exception ex) {
                    return null;
                }
            }   
        }

        public async Task<int> saveTableStores(table_stores store)
        {
            using (var db = new SuperZapatos.Data.Models.SuperZapatos())
            {
                try
                {
                    db.table_stores.Add(store);
                    return await db.SaveChangesAsync();
                }
                catch (Exception ex) {
                    return 0;
                }
            }
        }

        public async Task<int> updateTableStores(table_stores store)
        {
            using (var db = new SuperZapatos.Data.Models.SuperZapatos())
            {
                try
                {
                    var result = await db.table_stores.Where(x => x.id == store.id).FirstOrDefaultAsync();
                    if (result != null)
                    {
                        store.id = result.id;
                        db.table_stores.Attach(store);
                        db.Entry(store).State = EntityState.Modified;
                        return await db.SaveChangesAsync();
                    }
                    return 0;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public async Task<int> deleteTableStores(table_stores store) {
            using (var db = new SuperZapatos.Data.Models.SuperZapatos())
            {
                try
                {
                    var result = await db.table_stores.Where(x => x.id == store.id).FirstOrDefaultAsync();
                    if (result != null)
                    {
                        db.table_stores.Remove(result);
                        return await db.SaveChangesAsync();
                    }
                    return 0;
                }
                catch (Exception ex) {
                    return 0;
                }
            }
        }

        public async Task<List<table_stores>> getListTableStore()
        {
            using (var db = new SuperZapatos.Data.Models.SuperZapatos())
            {
                try
                {
                    var tem = await db.table_stores.ToListAsync();
                    return tem;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
    }
}
