﻿using SuperZapatos.Data.Models;
using SuperZapatos.Repository.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Data.Data
{
    public class TableArticles_Repository : ITableArticles_Repository<tablearticles>
    {
        public async Task<tablearticles> getTableArticles(tablearticles store)
        {
            using (var db = new SuperZapatos.Data.Models.SuperZapatos())
            {
                try
                {
                    var tem = await db.tablearticles.Where(x => x.id == store.id).FirstOrDefaultAsync();
                    return tem;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public async Task<int> saveTableArticles(tablearticles store)
        {
            using (var db = new SuperZapatos.Data.Models.SuperZapatos())
            {
                try
                {
                    db.tablearticles.Add(store);
                    return await db.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public async Task<int> updateTableArticles(tablearticles store)
        {
            using (var db = new SuperZapatos.Data.Models.SuperZapatos())
            {
                try
                {
                    var result = await db.tablearticles.Where(x => x.id == store.id).FirstOrDefaultAsync();
                    if (result != null)
                    {
                        store.id = result.id;
                        db.tablearticles.Attach(store);
                        db.Entry(store).State = EntityState.Modified;
                        return await db.SaveChangesAsync();
                    }
                    return 0;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public async Task<int> deleteTableArticles(tablearticles store)
        {
            using (var db = new SuperZapatos.Data.Models.SuperZapatos())
            {
                try
                {
                    var result = await db.tablearticles.Where(x => x.id == store.id).FirstOrDefaultAsync();
                    if (result != null)
                    {
                        db.tablearticles.Remove(result);
                        return await db.SaveChangesAsync();
                    }
                    return 0;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public async Task<List<tablearticles>> getListTableArticles()
        {
            using (var db = new SuperZapatos.Data.Models.SuperZapatos())
            {
                try
                {
                    var tem = await db.tablearticles.ToListAsync();
                    return tem;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


        public async Task<List<tablearticles>> getListTableArticlesxStore(long idStore)
        {
            using (var db = new SuperZapatos.Data.Models.SuperZapatos())
            {
                try
                {
                    var tem = await db.tablearticles.Where(x=> x.store_id == idStore).ToListAsync();
                    return tem;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        
    }
}
